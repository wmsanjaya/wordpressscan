<?php
define('WP_API_URL', 'http://api.wordpress.org/core/version-check/1.6/');
define('PLUGIN_URL','https://en-au.wordpress.org/plugins/');
define('WP_VERSION_REGEX', '#/wp-includes/version.php$#');
define('WP_VULNDB_API', 'http://wpvulndb.com/api/v2/');


define('WP_USE_THEMES', true);
define('BR',"<br/>");

$targetFolder = empty($_GET["path"]) ? '' : $_GET["path"];
if (empty($targetFolder)) {
	die("Usage: $argv[0] /path/to/folder\n");
}
$dir   = realpath(getcwd()."\\".$targetFolder);

p(printtitle("WORDPRESS REPORT"));
$latestWP  = getLatestVersion();
$currentWP = getCurrentVersion($dir);
$currVersion = $currentWP['version'];
$ltstversion = $latestWP['current'];

$vulurl = WP_VULNDB_API.'wordpresses/'.str_replace('.','',$currVersion);
$vulnarability = meCurl($vulurl);
$vul_obj = json_decode($vulnarability);
$vul_array = (array)$vul_obj;
$vul_array = (array)$vul_array[$currVersion];
$vul_array = $vul_array['vulnerabilities'];

if (empty($ltstversion)) {
	die("Failed to fetch latest version. Try again later.\n");
}
/* Wordpress Installation */
p(printtitle("Wordpress Installation","h3"));
p(printlb(),true);
p("Current Wordpress Version: ");
p($currVersion,true);
pr($currentWP);
if ($ltstversion > $currVersion) {
	p("",true);
	p("Upgrade the Wordpress Installation to the latest immediately", true);
	p("Latest Wordpress Version: ");
	p($ltstversion, true);
	p("Required PHP Version: ");
	p($latestWP['php_version'],true);
	p("Required MySQL Version: ");
	p($latestWP['mysql_version'],true);
} else {
	p("Wordpress Installation is up-to-date");
}
exit;

p(printtitle("Known Wordpress ".$currVersion." Vulnerabilities","h4"));
p(printlb(),true);
foreach ($vul_array as $vul) {
	p($vul->title,true);
}
/* Wordpress Installation */

/* Plugins */
p(printtitle("Installed Wordpress Plugins","h3"));
p(printlb(),true);
require ($dir.'/wp-load.php');
if ( ! function_exists( 'get_plugins' ) ) {
	require_once $dir.'/wp-admin/includes/plugin.php';
}

if ( ! function_exists( 'plugins_api' ) ) {
      require_once( $dir.'/wp-admin/includes/plugin-install.php' );
}

$all_plugins = get_plugins();
foreach ($all_plugins AS $plugin) {
	$slug = "";
	if ($plugin['TextDomain']) {
		$slug = $plugin['TextDomain'];
	} else {
		$slug = str_replace(' ','-',strtolower($plugin['Name']));
	}
	
	$args = array(
		'slug' => $slug,
		'fields' => array(
			'version' => true,
		)
	);
	$version_latest = "0.0.0";
	$call_api = plugins_api( 'plugin_information', $args );
	$plugin_file =  $slug."/".$slug.".php";
	
	$isactive = "NO";
	if( is_plugin_active( $plugin_file ) ) {
		$isactive = "YES";
	}
	
	if ( is_wp_error( $call_api ) ) {
		$api_error = $call_api->get_error_message()."<br/>";
	} else {		
		if ( ! empty( $call_api->version ) ) {
			$version_latest = $call_api->version;
		}
	}
	
	p(printtitle($plugin['Name'],"strong"));
	p(" ");
	p(PLUGIN_URL.$slug,true);	
	//pl("Description:",$plugin['Description'] );
	if ($plugin['Version'] ==$version_latest ) {
		pl("Installed:",$plugin['Version'] );
	} else {
		pl("Installed:",$plugin['Version'] );
		pl("Latest Version:",$version_latest );
	}
	
	if ( !is_wp_error( $call_api ) ) {
		//pl("Download Link:",$call_api->download_link);		
		//pl("Minimum Wordpress Version:",$call_api->requires);
		//pl("Tested Wordpress Version:",$call_api->tested);		
	}
	p(printtitle("Known vulnerabilities for plugin:".$plugin['Name'],"h4"));
	$vulurl = WP_VULNDB_API.'plugins/'.$slug;
	$vulnarability = meCurl($vulurl);
	$vul_obj = json_decode($vulnarability);
	$vul_array = (array)$vul_obj;
	$vul_array = (array)$vul_array[$slug];
	$vul_array = $vul_array['vulnerabilities'];	
	//pr($vul_array);
	if (count($vul_array) > 0 ) {
		foreach ($vul_array as $vul) {
			if ($vul->fixed_in > $plugin['Version']) {
				p($vul->title,true);
				p("Fixed in version:".$vul->fixed_in, true);
			}
		}
	} else {
		p("No reported vulnerabilities for the plugin", true);
	}
} 
/* Plugins */



/* Check edited files */
	p(printtitle("Edited Core Wordpress Files","h3"));
	p(printlb(),true);
	include( $dir.'/wp-includes/version.php' );
	$wp_locale = isset( $wp_local_package ) ? $wp_local_package : 'en_US';
	$apiurl = 'https://api.wordpress.org/core/checksums/1.0/?version=' . $wp_version . '&locale=' .  $wp_locale;
	$json = json_decode ( file_get_contents ( $apiurl ) );
	$checksums = $json->checksums;

	foreach( $checksums as $file => $checksum ) {
		$file_path = $dir.'/'.$file;

		if ( file_exists( $file_path ) ) {
			if ( md5_file ($file_path) !== $checksum ) {
				// do something when a checksum doesn't match
				p($file_path,true);
				if(preg_match('/^.*\.(php[\d]?|js|txt)$/i', $file_path)) { 
					check_files($file_path); 
				}
			}
		}
	}

/* Current Wordpress Template */
p(printtitle("Current Wordpress Template","h3"));
p(printlb(),true);
$my_theme = wp_get_theme();
pl("Name:",$my_theme->Name);
pl("Description:",$my_theme->Description);
pl("Version:",$my_theme->Version);
pl("ThemeURI:",$my_theme->ThemeURI);
pl("AuthorURI:",$my_theme->AuthorURI);
pr($my_theme);
$slug = $my_theme['headers:WP_Theme:private']['TextDomain'];
p(printtitle("Known vulnerabilities for Template:".$my_theme->Name,"h4"));
echo $vulurl = WP_VULNDB_API.'themes/'.$slug;
$vulnarability = meCurl($vulurl);
$vul_obj = json_decode($vulnarability);
$vul_array = (array)$vul_obj;
$vul_array = (array)$vul_array[$slug];
$vul_array = $vul_array['vulnerabilities'];	
pr($vul_array);
if (count($vul_array) > 0 ) {
	foreach ($vul_array as $vul) {
		if ($vul->fixed_in > $plugin['Version']) {
			p($vul->title,true);
			p("Fixed in version:".$vul->fixed_in, true);
		}
	}
} else {
	p("No reported vulnerabilities for the theme", true);
}


/* Current Wordpress Template */

/* CHECK Default Username Excists */
p(printtitle("Week Administrator Account Check","h3"));
p(printlb(),true);
$username = 'admin';
if ( username_exists( $username ) ) {
	echo "Not Secure: Change the default username:".$username;
} else {
	echo "Good that default username is not in use.";
}
/* CHECK Default Username Excists */

//find_files('./'.$targetFolder.'/');



function find_files($seed)
{
    if(! is_dir($seed)) return false;
	$files = array();
	$dirs = array($seed);
	while(NULL !== ($dir = array_pop($dirs)))
    {
        if($dh = opendir($dir))
        {
            while( false !== ($file = readdir($dh)))
            {
                if($file == '.' || $file == '..') continue;
                  $path = $dir . '/' . $file;
                  if(is_dir($path)) {    $dirs[] = $path; }                           
                  else { if(preg_match('/^.*\.(php[\d]?|js|txt)$/i', $path)) { check_files($path); }}                              
            }
            closedir($dh);
		}
	}
}

function check_files($this_file){
	$str_to_find[]='base64_decode';
	$str_to_find[]='edoced_46esab'; // base64_decode reversed
	$str_to_find[]='preg_replace';
	$str_to_find[]='HTTP_REFERER';   //  checks for referrer based conditions
	$str_to_find[]='HTTP_USER_AGENT';  //  checks for user agent based conditions
               
	if(!($content = file_get_contents($this_file))) { 
		echo("<p>Could not check $this_file You should check the contents manually!</p>\n"); 
	}else { 
        while(list(,$value)=each($str_to_find)){
            if (stripos($content, $value) !== false) { 
                echo("<p>$this_file -> contains $value</p>\n"); 
            }
        }
    }
    unset($content);
}

/**
 * Recursively search through a given directory to find all
 * WordPress version files.
 * 
 * @param string $dir Path to the top level foler
 * @return array List of found files
 */
/* function findVersionFiles($dir) {	
	$result = array();
	echo $dir   = realpath(getcwd()."\\".$dir);
	$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir));
	$Regex = new RegexIterator($objects, WP_VERSION_REGEX, RecursiveRegexIterator::GET_MATCH);
	

	$result[] = $dir."/wp-includes/version.php";
	asort($result);
	return $result;
} */
/**
 * Find WordPress installation location from version file
 * 
 * This simply chops off /wp-includes/version.php from the end of
 * the version file location.
 * 
 * @param string $file Location of the version file
 * @return string
 */
function getInstallationLocation($file) {
	$result = preg_replace(WP_VERSION_REGEX, '', $file);
	return $result;
}
/**
 * Fetch the information about latest version from WordPress.org
 * 
 * @return string latest WordPress version
 */
function getLatestVersion() {
	$result = '';
	$response = meCurl(WP_API_URL);;
	
	if (!empty($response)) {
		$response = unserialize($response);
	}
	
	
	if (!empty($response['offers'][0])) {
		$result = $response['offers'][0];
	}
	return $result;
}


function getCurrentVersion($dir){
	$result = array();
	$file = $dir."/wp-includes/version.php";
	if (!file_exists($file)) {
		print "Did not find any WordPress version files in $file\n";
		exit;
	}

	$result['location'] = getInstallationLocation($file);
	$result['version'] = getInstalledVersion($file);			
	return $result;
}

/**
 * Figure out installed WordPress version from the file
 * 
 * @param string $file WordPress version file
 * @return string
 */
function getInstalledVersion($file) {
	$result = '';
	unset($wp_version);
	require_once $file;
	if (isset($wp_version)) {
		$result = $wp_version;
	}
	return $result;
}

function meCurl($url) {
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_FAILONERROR, true); 
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	$response = curl_exec($curl);
	
	if($errno = curl_errno($curl)) {
		$error_message = curl_strerror($errno);
		echo "cURL error ({$errno}):\n {$error_message}";
	}
	
	curl_close($curl);
	return $response;
}

function printlb() {
	return "====================================";
}

function printtitle($text, $hx = "h1") {
	$str = "";
	$str .= htmltag($hx);
	$str .= $text;
	$str .= htmltag($hx,true);
	return $str;
}

function htmltag($tag, $isclose = false) {
	if ($isclose) 
		return '</'.$tag.'>';
	return '<'.$tag.'>';		
}

function p($str, $isnl=false){	
	echo $str;	
	if ($isnl) echo BR;	
}

function pl($str1, $str2) {
	p($str1);
	p($str2,true);
}

function pr($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}
?>